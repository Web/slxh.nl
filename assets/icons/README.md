Icons in this directory are sources from the following projects:

- [Feather](https://github.com/feathericons/feather) (MIT license)
- [Simple Icons](https://github.com/simple-icons/simple-icons) (CC0-1.0 license)
