---
title: NAT64 performance
date: 2021-03-31T20:07:06+02:00
draft: false
aliases:
  - /blog/NAT64 performance
tags:
  - router
  - ipv6
  - nat64
  - siit
---

This post is an update of [my previous post about NAT64](/blog/NAT64%20for%20servers).
That post describes the following setup:

```
          IPv4          IPv4
Internet ────── Router ────── NAT64
          IPv6    │             │ IPv6
                  └────────── Host
                        IPv6
```

<!--more-->

This has the following components in the network:

- A *router* that routes IPv6 and routes or NATs IPv4.
- A *NAT64* server that performs thanslation of IPv4 and IPv6.
- A *host* that only has IPv6 connectivity.

The is to use the NAT64 server both ways:
the NAT64 server is used for IPv4 connectivity (looking at you, GitHub),
and to reach the IPv6-only hosts over IPv4.

Currently, I still use [Tayga][] for this setup.
The main downside of Tayga is it's (low) performance.
This performance is the main reason various hosts in my network still have
an IPv4 address.
I have also replaced my home router with a [PCEngines APU][],
which should perform much better than the Edgerouter Lite it replaced.

## Alternatives

There are various alternatives to Tayga:

- OpenBSD's [PF][] (using [af-to][]).
- [Jool][] can perform both (stateful) NAT64 and (stateless) [SIIT][].
- [map646][] only performs stateless NAT64.

The latter two have been compared against Tayga in [this paper][paper],
which found the following performance:

| Solution | frame/s | Mbps (est.) |
|----------|--------:|------------:|
| Jool     |  230000 |        2760 |
| Tayga    |  120000 |        1440 |
| map646   |   60000 |         720 |

Note that the throughput for Tayga is significantly greater than I was able to
achieve in my previous post (252 Mbps).
Additionally, the performance of Jool looks *interesting*...

## Test setup

The following devices are used for testing the performance of Tayga, Jool and PF:

|     | PCEngines APU      | Ryzen 2600X           | Ryzen 4650G |
|-----|:-------------------|:----------------------|:------------|
| CPU | [AMD GX-412][]     | [AMD Ryzen 5 2600X][] | [AMD Ryzen 5 PRO 4650G][]
| NIC | [Intel i211AT][]   | [Intel X550-T2][]     | [Mellanox ConnectX-3 EN][]
| RAM | 2GB DDR3-1333      | 32GB DDR4-3200        | 32GB DDR4-3200

Both the PCEngines APU and the Ryzen2600X are used as NAT64 device,
while the Ryzen 4650G is used to measure performance.

Fresh installations of OpenBSD 6.8 and Debian Bullseye are used for
the tests. No performance tuning is done.

Tests are performed using iPerf3 using a single TCP flow
on a single link (in full duplex).
The best of three measurements is used as the result.

A few notes:

- Note that these numbers do not include overhead from Ethernet, IP or TCP (5-6%).
- The network, and the devices, may be somewhat in use.
- Only a single full-duplex link is used.

This means that the results below should be taken with a grain of salt.

## Different types of NAT

Tayga, Jool and PF work differently:

- Tayga and PF (and Jool in NAT64 mode) masquerade the original IP.
  Tayga can do so using an (optional) dynamic pool, and static mappings.

- PF also masquerades the IPv6 address in the NAT46 flow.

- Jool (in SIIT mode) does a stateless translation between IPs.
  This means that the original IP also has to be one from the pool.

- Jool (in SIIT mode) translates *all* IPv4 traffic.

Example translations for NAT64:

|       | Orig From           | Orig To             | NAT From     | NAT To     |
|-------|---------------------|---------------------|--------------|------------|
| Tayga | *                   | 64:ff9b::10.64.0.10 | 10.64.0.0/24 | 10.64.0.10 |
| Jool  | 64:ff9b::10.64.1.10 | 64:ff9b::10.64.0.10 | 10.64.1.10   | 10.64.0.10 |
| PF    | *                   | 64:ff9b::10.64.0.10 | 10.64.0.1    | 10.64.0.10 |

Example translations for NAT46:

|       | Orig From  | Orig To    | NAT From             | NAT To               |
|-------|------------|------------|----------------------|----------------------|
| Tayga | 10.64.1.10 | 10.64.0.10 | 2001:db8::10.64.1.10 | 2001:db8::10.64.0.10 |
| Jool  | 10.64.1.10 | 10.64.0.10 | 2001:db8::10.64.1.10 | 2001:db8::10.64.0.10 |
| PF    | *          | 10.64.0.10 | 2001:db8::1          | 2001:db8::10.64.0.10 |

## Results

Performance on the PCEngines APU in Mbps:

|       | IPv4 | IPv6 | NAT64 | NAT46 |
|:------|-----:|-----:|------:|------:|
| Tayga |  903 |  887 |   199 |   197 |
| Jool  |    - |  869 |   805 |   825 |
| PF    |  646 |  488 |   572 |   444 |

Performance on the Ryzen 2600X in Mbps:

|       | IPv4 | IPv6 | NAT64 | NAT46 |
|:------|-----:|-----:|------:|------:|
| Tayga | 8478 | 8361 |  2971 |  2807 |
| Jool  |    - | 7819 |  8193 |  8091 |
| PF    | 6292 | 5006 |  2354 |  5440 |

As a bonus: performance of Jool in a container on the Ryzen 4650G in Mbps:

|       | IPv4  | IPv6  | NAT64 | NAT46 |
|:------|------:|------:|------:|------:|
| Jool  | 15216 | 14406 | 14426 | 10097 |

This means the performance of the solutions,
when compared to the line rate,
is as follows:

| Solution | Platform      | NAT64  | NAT46   |
|:---------|:--------------|-------:|--------:|
| Tayga    | PCEngines APU | 21%    | 21%     |
| Tayga    | Ryzen 2600X   | 31%    | 29%     |
| PF       | PCEngines APU | 50%    | 47%     |
| PF       | Ryzen 2600X   | 25%    | 58%     |
| Jool     | PCEngines APU | 85%    | 88%     |
| Jool     | Ryzen 2600X   | 88%    | 86%     |

## Conclusion

While Tayga claims that it "can saturate gigabit Ethernet on modest PC hardware",
this is clearly not the case for the 'modest' PCEngines APU.
However, on a modern desktop CPU all solutions were able to attain performance
exceeding 1 Gbps.

The clear winner in terms of performance is Jool,
which is able to achieve almost 90% line rate on both platforms.

<!--
## Appendix: configurations

The following sections contain example configurations.

### Tayga

`/etc/default/tayga`:

```
# Configure NAT44 for the private IPv4 range
CONFIGURE_NAT44="no"
```

`/etc/tayga.conf`

```
tun-device   nat64
prefix       2001:db8::/96
ipv4-addr    10.64.0.1
dynamic-pool 10.64.0.0/24
data-dir     /var/spool/tayga

map 10.64.0.10 2001:db8:1::10
```

### Jool

```console
jool_siit instance add siit \
  --netfilter \
  --pool6 2001:db8::/96
```

### PF

```conf
# Translate all IPv4 traffic by adding a NAT46 prefix
pass in quick on ix1 inet from any to 10.64.0.0/16 \
  af-to inet6 from 2001:db8::1 to 2001:db8::/112

# Translate all IPv6 traffic to IPv4
pass in quick on ix1 inet6 from any to 64:ff9b::/96 \
  af-to inet from 10.64.0.1
```

-->

[PF]: https://www.openbsd.org/faq/pf/filter.html
[af-to]: https://man.openbsd.org/pf.conf.5#af-to
[NAT64]: https://en.wikipedia.org/wiki/NAT64
[Tayga]: http://www.litech.org/tayga/
[ipv6-only]: https://blog.luukhendriks.eu/2015/12/01/ipv6-only-network-at-home.html
[apu]: https://www.pcengines.ch/apu2.htm
[ryzen]: https://www.amd.com/en/products/apu/amd-ryzen-5-pro-4650g
[ryzen2]: https://www.amd.com/en/products/cpu/amd-ryzen-5-2600x
[Jool]: https://www.jool.mx
[SIIT]: https://tools.ietf.org/html/rfc7915
[map646]: https://github.com/keiichishima/map646
[paper]: https://link.springer.com/article/10.1007/s11235-020-00681-x

[PCEngines APU]: https://www.pcengines.ch/apu3c2.htm
[AMD GX-412]: https://www.amd.com/en/system/files?file=2017-06/g-series-soc-product-brief.pdf
[AMD Ryzen 5 2600X]: https://www.amd.com/en/products/cpu/amd-ryzen-5-2600x
[AMD Ryzen 5 PRO 4650G]: https://www.amd.com/en/products/apu/amd-ryzen-5-pro-4650g
[Intel i211AT]: https://ark.intel.com/content/www/us/en/ark/products/64404/intel-ethernet-controller-i211-at.html
[Intel X550-T2]: https://ark.intel.com/content/www/us/en/ark/products/88209/intel-ethernet-converged-network-adapter-x550-t2.html
[Mellanox ConnectX-3 EN]: https://www.mellanox.com/products/ethernet-adapters/connectx-3-pro
