---
title: Nuclear energy in the Netherlands
date: 2024-06-15T15:01:52+02:00
draft: true
---

There is an active debate in The Netherlands about building nuclear energy.
This article attempts to provide a summary of the reasons for/against nuclear energy.
Please let me know if you feel that facts have been misrepresented or are out of date.

First, let's state the goal of a nuclear power plant:

> A nuclear power plant should provide a reliable and cheap base load with zero CO₂ emissions in an energy independent (of non-EU countries) manner and be [completed in 2035][0].

Discussions usually tend to focus on one of the talking points below,
which I have split along the lifecycle of a nuclear power plant.
I have purposfully skipped over the CO₂ emissions and safety of a nuclear power plant,
as these do not seem to be contentious topics.

## Building a nuclear power plant

### Who is going to build it?

[There are no great options][1]:

- [Rosatom], which is a Russian state corporation.
  Consider that energy independence from Russia is a major driver behind the push to
  alternative energy sources (because of the invasion of Ukraine).

- [China General Nuclear Power Group (CGN)][CGN], which is a state-owned Chinese company.
  Consider, again, that energy independence is one of the stated goals of a nuclear energy.

- [Korea Electric Power Corporation (KEPCO)][KEPCO], a Korean company.
  They have delivered a large amount of nuclear power pants in Korea and
  are currently building one in the United Arab Emirates.
  They have not been involved in building a plant in Europe (yet).
  KEPCO has been involved in [falsifying safety documents][2].

- [Framatome]/[EDF], a French company.
  Famatome has had [quality control issues][3] in the past,
  and their only operational reactor design ([EPR]) has been completely redone because it was too complicated.
  There is currently a single project for building these new reactors in the planning phase.

- Start from scratch:
  Design our own nuclear reactor with the expertise that we still have in the Netherlands.
  I don't think anybody considers this a realistic option at this point.

This means that there is no proven design available that we can and want to build.
It is possible to build a power plant with either KEPCO or Framatome/EDF *if* we really want to commit to nuclear energy, but this is going to require faith in the new player and design respectively.

This overview skips over small moduler reactor (SMR) designs, which are not currently operational outside of experiments in China and Russia.

### How long is it going to take to build one?

The short answer is that *no one knows*:
Most projects in Europe were built using the complicated EPR design,
so any other design could be completed faster.

The following table show the power plants projects in the EU and UK in the last 25 years:

| Name | Status | Finished | Builder | Duration | Cost |
|------|--------|----------|---------|---------:|-----:|
| [Civaux][4] | Completed | 2002 | EDF | 16 years | $4.1 billion |
| [Olkiluoto 3][7] | Completed | 2023 | Areva/TVO | 18 years | €11 billion |
| [Mochovce 3/4][8] | Construction | 2023-2025 | Slovenské elektrárne | 22 years | €4.6 billion |
| [Flamanville 3][6] | Construction | 2024 | EDF | 17 years | €8.5 billion |
| [Hinkey Point C][5] | Construction | 2029-2031 | EDF/CGN | 16-18 years | £31–35 billion |

Based on this it takes at least 15 years between government authorisation and completion.
Additionally, there needs to be some time for government planning, tenders, etc.

Realistically, this means that it takes 20 years to actually construct a nuclear power plant.

## Operating a nuclear power plant

### Cost vs renewables

While the operating costs of a nuclear power plant are fairly low,
the [cost of electricity][10] includes construction and decomissioning.
Because of this, there is often fixed or guaranteed minimum price for nuclear energy.
This used to be the case for offshore wind, but is no longer the case with the latest tenders.

The following graph ([source][11]) shows the learning curves for various energy sources:

![learning curves](/blog/files/learning-curves.svg)

Note that solar panels and wind both trend down, while nuclear energy trends upwards.
This is also reflected in the latest real numbers:

| Name       | Kind    | Year | Price/MWh |
|------------|---------|-----:|------|
| Mochovce 3 | Nuclear | 2023 | [€61][9] |
| Borssele IV | Offshore wind | 2016 | [€55][11] |

And estimates for 2030:

| Kind             | Price/MWh      |
|------------------|----------------|
| Nuclear          | [€50-€100][14] |
| Offshore wind    | [€30-€40][13]  |
| Onshore wind     | [€40-€60][13]  |
| Solar panels     | [€30-€60][13]  |

Considering we are talking about the *base load*, energy storage estimates for 2030 should also be taken into account ([source][18]):

| Kind         | Duration | Price/MWh |
|--------------|---------:|----------:|
| Battery      | 1 h      | €38-€106  |
| Battery      | 8 h      | €76-€218  |
| Battery      | 2000 h   | >€1000    |
| Pumped hydro | 1 h      | €18-€28   |
| Pumped hydro | 8 h      | €24-€42   |
| Pumped hydro | 2000 h   | >€400     |
| Hydrogen     | 2000 h   | €140      |

Reports mixing these prices depending on various energy distributions come to similar conclusions:

- [KIVI estimates][14] that the combination of hydrogen storage and offshore wind comes out to €65-€78 per MWh, equivalent to their estimate for nuclear energy.

- [Kalavasta & Berenschot][15] come to the conclusion that nuclear energy is slightly (€1-3 billion/year) more expensive in most scenarios.

- The [latest scenario study][16] comissioned by the government reports that nuclear energy is only really viable in the most optimistic scenarios. The authors also note that increased investments in nuclear energy show a decrease in investements in renewables.


### Base load

The concept of a base load comes from our current use of energy in which we have a fairly predictable pattern with a [base and peak demand][33] of electricity.

However, this is not necessarily a given:
Dynamic energy pricing can shift the use of electricity to better match supply:
Considering that [overcapacity is inevitable][37], and that households make up only [29% of electricity consumption][36], businesses may have a strong incentive to change their usage patterns.

### Raw materials

The following image from [latest scenario study][16] shows the geopolitical risk for aquiring the raw materials for energy sources:

![raw material risks](/blog/files/energy-raw-material-risks.png)

The risk for nuclear energy is similar to wind, but a lot higher than Solar and battery storage.

It is important to note that we can enrich our own uranium in the Netherlands, so only the procurement of the uranium is an issue.

### Waste

We currently [do not have solution][14] for storing nuclear waste,
but there are a few options:

- Store it in clay anywhere in the Netherlands.
- Storing it in halite in the north of the Netherlands.
  This might not be a politically viable option considering the current handling of gas extraction and waste water storage in those regions.

There are no actual plans, which means that the costs are extremely unclear.
This also impacts the costs above.

## Conclusion

If we look back at the stated goals for a nuclear power plant:

- Providing a reliable base load is possible, but might not be needed.
  Nuclear energy is at least as expensive as renewables combined with storage.

- It would provide energy with (close to) zero CO₂ emissions.
  (I have skipped passed this issue above as it is not really contentious).

- Energy independence is possible if we don't let China or Russia build/operate the power plants.
  This, however, requires the use of unproven reactor designs.

- Having a nuclear reactor operational in 2035 is not feasible.
  Even if we start today, it is unlikely that the power plant will provide energy in 2040.

Looking at the data raises the following political questions:

- Should the government assume the (financial) risk of building a nuclear power plant?
- Should the government invest in unproven nuclear energy *instead of* proven renewables?
- Is having a reliable base load needed if we change how and when we use electricity?

### Other talking points and questions

I often hear or see the following talking points in discussions around nuclear energy:

> "The best time to build a nuclear reactor was 20-40 years ago, the second best time is now."

This does not take into account that renewables might actually be a better option than nuclear energy *now*.

It is also debatable if it was a good idea 20-40 years ago,
considering the [dumping of nuclear waste in oceans][19] (1946-1993),
various incidents caused by design issues ([1979][20], [1986][21], [1989][22], [1999][23], [2011][23]),
and the fact that we haven't solved the problem with waste in the last 60(!) years.

Not to mention that both [Hinkley Point C][5] and [Flamanville 3][6] started about 20 years ago,
and have been plagued with cost overruns and delays.

> "An investment into nuclear energy is an investment in Dutch expertise in the field."

This is only the case if we build our own design (see above),
which is unlikely given the time constraints.
Also: why not invest in science directly if we want to invest in expert knowledge?

> "The sun does not always shine and the wind does not always blow,
> but a nuclear power plant always delivers."

This is an anti-renewables talking point used in favour of nuclear energy.

It somewhat misunderstands the challenge which is not with base load, but [peak demand][33].
Any problems with base load can be remedied by improving [the European power grid][30] (European weather is fairly predictable on average).
Note that there is a [low inverse correlation][31] between solar and wind in the long term,
and some European countries have [more wind at night][32].

[0]: https://nos.nl/artikel/2511577-tweede-kamer-wil-vier-grote-kerncentrales-in-plaats-van-twee
[1]: https://berthub.eu/articles/posts/het-gaat-om-de-kerncentrales/
[2]: https://www.world-nuclear-news.org/RS-Indictments_for_South_Korea_forgery_scandal-1010137.html
[3]: https://www.energy-reporters.com/industry/edf-admits-to-reactor-welding-issues-in-france/
[4]: https://www.power-technology.com/projects/civaux/
[5]: https://en.wikipedia.org/wiki/Hinkley_Point_C_nuclear_power_station
[6]: https://en.wikipedia.org/wiki/Flamanville_Nuclear_Power_Plant#Unit_3
[7]: https://en.wikipedia.org/wiki/Olkiluoto_Nuclear_Power_Plant
[8]: https://en.wikipedia.org/wiki/Mochovce_Nuclear_Power_Plant
[9]: https://world-nuclear-news.org/Articles/Mochovce-3-supplies-first-electricity-to-grid
[10]: https://en.wikipedia.org/wiki/Economics_of_nuclear_power_plants
[11]: https://en.wikipedia.org/wiki/Cost_of_electricity_by_source
[12]: https://windopzee.nl/onderwerpen/wind-zee/kosten/kosten-windparken
[13]: https://windopzee.nl/publish/pages/225578/-digital-dutch-offshore-wind-market-report_21092023.pdf
[14]: https://www.kivi.nl/uploads/media/5fdb546e6a6f7/eRisk-Group_rapport_De-rol-van-kernenergie_20201202-1.pdf
[15]: https://kalavasta.com/assets/reports/Nucleaire_variant_Europees_Scenario_II3050_Rapport_FINAL.pdf
[16]: https://open.overheid.nl/documenten/ronl-46fb6f84d40d2ed22a4db4709e932d03f53b82c2/pdf
[17]: https://energy.ec.europa.eu/system/files/2019-06/report-_battery_storage_to_drive_the_power_system_transition_0.pdf
[18]: https://energy.ec.europa.eu/system/files/2017-02/swd2017_61_document_travail_service_part1_v6_0.pdf
[19]: https://en.wikipedia.org/wiki/Ocean_disposal_of_radioactive_waste
[20]: https://en.wikipedia.org/wiki/Three_Mile_Island_accident
[21]: https://en.wikipedia.org/wiki/Chernobyl_disaster
[22]: https://en.wikipedia.org/wiki/Vandell%C3%B2s_Nuclear_Power_Plant
[23]: https://en.wikipedia.org/wiki/Tokaimura_nuclear_accidents
[24]: https://en.wikipedia.org/wiki/Fukushima_nuclear_accident
[30]: https://www.sciencefiguredout.be/how-make-renewable-energy-flow-through-europe
[31]: https://filelist.tudelft.nl/user_upload/MREL_D6.1_EUSCORES.pdf
[32]: https://carboncounter.wordpress.com/2015/07/10/are-wind-farms-more-productive-at-night/
[33]: https://en.wikipedia.org/wiki/Peak_demand
[34]: https://www.sciencedirect.com/science/article/pii/S0960148123006018
[36]: https://www.enerdata.net/estore/energy-market/european-union/
[37]: https://www.power-technology.com/features/featureovercapacity-and-the-challenges-of-going-100-renewable-5872868/
[Rosatom]: https://en.wikipedia.org/wiki/Rosatom
[CGN]: https://en.wikipedia.org/wiki/China_General_Nuclear_Power_Group
[Framatome]: https://en.wikipedia.org/wiki/Framatome
[EDF]: https://en.wikipedia.org/wiki/%C3%89lectricit%C3%A9_de_France
[KEPCO]: https://en.wikipedia.org/wiki/Korea_Electric_Power_Corporation
[EPR]: https://en.wikipedia.org/wiki/EPR_(nuclear_reactor)
[EPR2]: https://en.wikipedia.org/wiki/EPR_(nuclear_reactor)#EPR2_design
