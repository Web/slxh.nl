---
date:  2016-11-23 16:06:17.895081251 +0100
title: NAT64 for servers
draft: false
aliases:
  - /blog/NAT64 for servers
tags:
  - router
  - ipv6
  - nat64
  - siit
---
This post details the setup of the additional steps required to deploy a IPv6-only server bedind [NAT64][] with [Tayga][].

<!--more-->

This post uses the *documentation prefix* (`2001:db8::/32`),
replace this with your own prefix.
This can be a ULA prefix.

The basis is explained in a post by [Luuk Hendriks][ipv6-only].

Goal
----
The goal is to reach an IPv6-only webserver behind a dual stack router.
This is done by using NAT64 on a third device to reach the server.

```
          IPv4          IPv4
Internet ────── Router ────── Tayga
          IPv6    │             │ IPv6
                  └────────── Server
                        IPv6
```

In this case, the Router is an *Edgerouter Lite* and
the Tayga and webserver are running on the same physical device in separate LXC containers.

Unbound
-------
The main difference in the setup is that I use Unbound instead of PowerDNS.
The configuration is really simple, in `/etc/unbound/unbound.conf.d/dns64.conf`:

```
server:
  module-config: "dns64 validator iterator"
    dns64-prefix: 2001:db8:64::/96
```

Tayga
-----
For the two-way NAT, the Tayga configuration requires static-mappings:

```
# Main config
tun-device   nat64
prefix       2001:db8:64::/96
ipv4-addr    10.64.0.1
dynamic-pool 10.64.0.0/24
data-dir     /var/spool/tayga

# Static mappings
map 10.64.0.10 2001:db8::10 # Server 1
map 10.64.0.11 2001:db8::11 # Server 2
```

I have used a subnet (`2001:db8:64::/96`) above as prefix.
This allow access to local (RFC1918) IPv4 addresses through NAT64.
If this is not needed, use the *Well-Known Prefix*: `64:ff9b::/96`.

The mapping is simple: create an IPv4 address inside the *dynamic pool* and map that to the IPv6 address that needs to be reached.

Route
-----
In a regular setup, the IPv4 range is only required internally on the host running Tayga.
For this setup, however, the `10.64.0.0/24` range needs to be routed to the device running Tayga.

Just like the [linked post][ipv6-only], I use an Edgerouter.
The following route needs to be added:

```
set protocols static route 10.64.0.0/24 next-hop 10.0.0.64 distance 1
```

Where `10.0.0.64` is the IPv4 address of the device running Tayga.

After this route is setup, it should be possible to ping the devices mapped in Tayga.

Port forwards
-------------

If the mappings work, it should now be possible to forward ports to the mapped addresses.
For a webserver this would be:

```
edit port-forward rule 10
set description "HTTP(S)"
set forward-to address 10.64.0.10
set original-port 80,443
set protocol tcp
```

If everything went well the webserver should now be reachable from the IPv4-WAN address,
while only having an IPv6 address.

Final notes
-----------
I have not noticed any drawbacks to this setup except for performance.
The results of some measurements from a computer in my network to the server show that the NAT transition has a performance of about 250 Mbps.

| From | To   | (Mbps) |
|------|------|------|
|      | IPv4 | IPv6 |
| IPv4 |  927 |  252 |
| IPv6 |  199 |  918 |

This means that the main bottleneck is the performance of Tayga,
though this only matters with internet connections faster than 250 Mbps.

 [NAT64]: https://en.wikipedia.org/wiki/NAT64
 [Tayga]: http://www.litech.org/tayga/
 [ipv6-only]: https://blog.luukhendriks.eu/2015/12/01/ipv6-only-network-at-home.html
