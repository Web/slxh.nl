---
title: LaTeX
---
In this place I've tried to make an overview of all (somewhat public) {{<latex>}} Stuff I've created.
You can also check out my [CTAN](http://www.ctan.org/author/hofstra) or [GitHub](https://github.com/silkeh/) profile.

## Stable {{<latex>}} Packages {#stable-latex-packages}
Compressed packages follow the [Tex Directory Structure (TDS)][2], you can unpack them directly into your `texmf` directory.

### sourcesanspro
[CTAN](http://www.ctan.org/pkg/sourcesanspro),
[Documentation](https://github.com/silkeh/latex-sourcesanspro/blob/master/doc/latex/sourcesanspro/sourcesanspro.pdf),
[Releases](https://github.com/silkeh/latex-sourcesanspro/releases).

Adobe's first open source font family.

### sourcecodepro
[CTAN](http://www.ctan.org/pkg/sourcecodepro),
[Documentation](https://github.com/silkeh/latex-sourcecodepro/blob/master/doc/latex/sourcecodepro/sourcecodepro.pdf),
[Releases](https://github.com/silkeh/latex-sourcecodepro/releases).

Adobe's first open source monospaced font.

### sourceserifpro
[CTAN](http://www.ctan.org/pkg/sourceserifpro),
[Documentation](https://github.com/silkeh/latex-sourceserifpro/blob/master/doc/latex/sourceserifpro/sourceserifpro.pdf),
[Releases](https://github.com/silkeh/latex-sourceserifpro/releases).

Adobe's first open source serif font.

### raleway
[CTAN](http://www.ctan.org/pkg/raleway),
[Documentation](https://github.com/silkeh/latex-raleway/blob/master/doc/latex/raleway.pdf),
[Releases](https://github.com/silkeh/latex-raleway/releases).

Matt McInerney's Raleway family.

### comicneue
[CTAN](http://www.ctan.org/pkg/raleway),
[Documentation](https://github.com/silkeh/latex-comicneue/blob/master/doc/latex/comicneue/comicneue.pdf),
[Releases](https://github.com/silkeh/latex-raleway/releases).

Comic Sans, redesigned!

## Other Projects
An overview can also be found on [my local Git repositories](https://git.slxh.eu/tex).

### siltex

Personal package for nicely layed out documents. [Download from Git](https://git.slxh.eu/tex/siltex.git).

### utwente

Templates for University of Twente projects. [Download from Git](https://git.snt.utwente.nl/visual-identity/latex-utwente.git).

[1]: https://upload.wikimedia.org/wikipedia/commons/9/92/LaTeX_logo.svg
[2]: http://www.tex.ac.uk/tex-archive/tds/tds.html
