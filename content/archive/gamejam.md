---
title: Gamejam
---
# Last Minute Madness: Retribution
A multiplayer game made the the second IA gamejam of 2015.

This game was made with [HaxeFlixel](http://haxeflixel.com/).

![Screenshot](https://www.slxh.eu/files/ia-gamejam-2015.2/screenshot.png)

## Download

 * [Linux (32-bit)](https://www.slxh.eu/files/ia-gamejam-2015.2/game-linux32.zip)
 * [Linux (64-bit)](https://www.slxh.eu/files/ia-gamejam-2015.2/game-linux64.zip)
 * [Mac OSX (32-bit)](https://www.slxh.eu/files/ia-gamejam-2015.2/game-mac32.zip)
 * [Mac OSX (64-bit)](https://www.slxh.eu/files/ia-gamejam-2015.2/game-mac64.zip)
 * [Windows (multi)](https://www.slxh.eu/files/ia-gamejam-2015.2/game-win.zip)

# Last Minute Madness
A game made for the IA Gamejam 2015.

This game was made with [LÖVE](https://love2d.org/).

![Screenshot](https://www.slxh.eu/files/ia-gamejam-2015/screenshot.png)

## Download

 * [LÖVE file (for all platforms)](https://www.slxh.eu/files/ia-gamejam-2015/game.love)
 * [Mac OSX (64-bit)](https://www.slxh.eu/files/ia-gamejam-2015/game-mac64.zip)
 * [Windows (32-bit)](https://www.slxh.eu/files/ia-gamejam-2015/game-win32.zip)
 * [Windows (64-bit)](https://www.slxh.eu/files/ia-gamejam-2015/game-win64.zip)

## Awards

![Award Photo](https://www.slxh.eu/files/ia-gamejam-2015/awardphoto.jpg)
