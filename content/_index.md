---
title: Home
menu: main
weight: 1
aliases:
  - /home
---

This is the personal website of Silke Hofstra (he/him),
an Embedded Software Engineer from The Netherlands.

You can find/reach me on
[{{< brand gitlab >}}&nbsp;GitLab](https://gitlab.com/silkeh),
[{{< brand github >}}&nbsp;GitHub](https://github.com/silkeh),
[{{< brand keybase >}}&nbsp;Keybase](https://keybase.io/silke),
[{{< brand matrix >}}&nbsp;Matrix](https://matrix.to/#/@silex:slxh.eu)
and [{{< icon mail >}}&nbsp;e-mail](mailto:contact@slxh.nl).

In my spare time I do some hiking,
and I sometimes write down some experience/experiments below.
