#!/usr/bin/env bash
set -euo pipefail

# Default arguments for Hugo
HUGO_ARGS=()
if [ -z ${HUGO_ARGS+x} ]
then
    HUGO_ARGS+=(--quiet --minify --cleanDestinationDir)
fi

if [ -n "${SITE_DIR+x}" ]
then
    HUGO_ARGS+=("--destination=${SITE_DIR}")
fi

if [ -n "${BASE_URL+x}" ]
then
    HUGO_ARGS+=("--baseURL=${BASE_URL}")
fi

IPFS_ARGS=()
if [ -n "${IPFS_API+x}" ]
then
    IPFS_ARGS+=("--api=${IPFS_API}")
fi

_ipfs() {
    ipfs "${IPFS_ARGS[@]}" "$@"
}

for arg in "$@"
do
    echo "Step: ${arg}"
    case "${arg}" in
        hugo)
            hugo "${HUGO_ARGS[@]}"
            ;;
        ipfs)
            id="$(_ipfs id -f"<id>")"
            cur="$(_ipfs name resolve --nocache "/ipns/${id}" || true)"
            new="/ipfs/$(_ipfs add -Qr public/)"
            if [ "${cur}" == "${new}" ]
            then
                echo "Already published: ${new}"
            else
                _ipfs name publish "${new}"

                if [ -n "${cur}" ]
                then
                    echo "Unpinning: ${cur}"
                    _ipfs pin rm "${cur}"
                fi
            fi
            ;;
    esac
done
